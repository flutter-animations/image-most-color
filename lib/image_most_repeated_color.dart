import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart' as img;

class ImageMostRepeatedColor extends StatefulWidget {
  const ImageMostRepeatedColor({super.key});

  @override
  State<ImageMostRepeatedColor> createState() => _ImageMostRepeatedColorState();
}

class _ImageMostRepeatedColorState extends State<ImageMostRepeatedColor> {
  Uint8List? image;
  Color? _color;
  bool loading = false;

  Future<void> pickImage() async {
    setState(() {
      loading = true;
    });

    await ImagePicker()
        .pickImage(source: ImageSource.gallery)
        .then((pickedImage) async {
      //define the image file
      final file = File(pickedImage!.path);

      //retrieve image data list
      final data = await file.readAsBytes();

      /*
      decrease the resolution of the image 
      in order to decrease the decoding process requirements
      */
      image = await FlutterImageCompress.compressWithList(data,
          minHeight: 800, minWidth: 600);

      //decoding the image Data
      final imageData = img.decodeJpg(image!);

      //fetching image width and height
      final width = imageData?.width ?? 0;
      final height = imageData?.height ?? 0;
      final colors = [];

      for (int i = 0; i < width; i += 10) {
        for (int j = 0; j < height; j += 10) {
          //get every pixel's color's four elemnt
          final pixel = imageData?.getPixel(i, j);
          final alpha = pixel!.a;
          final red = pixel.r;
          final blue = pixel.b;
          final green = pixel.g;

          final color = Color.fromARGB(
              alpha.toInt(), red.toInt(), green.toInt(), blue.toInt());

          colors.add(color);
        }
      }

      //determinate the domaint color
      Color? domainatColor;
      int maxCount = 0;
      Map<Color, int> colorsCount = {};

      for (Color color in colors) {
        int count = colorsCount[color] ?? 0;
        colorsCount[color] = count + 1;
        if (count + 1 > maxCount) {
          maxCount = count + 1;
          domainatColor = color;
        }
      }

      setState(() {
        _color = domainatColor;
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: AnimatedContainer(
        duration: const Duration(milliseconds: 200),
        color: _color,
        width: size.width,
        height: size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: InkWell(
                borderRadius: BorderRadius.circular(20),
                onTap: pickImage,
                child: Container(
                  width: size.width * 0.8,
                  height: size.width * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 32,
                        offset: const Offset(0, 6),
                        color: Colors.black.withOpacity(0.1),
                      ),
                    ],
                  ),
                  child: image != null || loading
                      ? Builder(
                          builder: (context) {
                            if (loading) {
                              return const Padding(
                                padding: EdgeInsets.all(150),
                                child: CircularProgressIndicator(),
                              );
                            }
                            return ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: Image.memory(
                                image!,
                                fit: BoxFit.cover,
                              ),
                            );
                          },
                        )
                      : const Center(
                          child: Text(
                            'Pick an image',
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
